const AWS = require('aws-sdk'),
    fs = require('fs-extra');

const Polly = new AWS.Polly({
    signatureVersion: 'v4',
    region: 'us-east-1'
});

function checkParams(param, param_name) {
    if (typeof param === 'undefined') {
        throw new Error(`Mandatory parameter ${param_name} is not defined.`);
    }
}

async function createMP3({
        text_to_record, 
        file_name, 
        voice_id=`Joanna`,
        language_code = `en-US`,
        audio_folder = `./mp3_files`,
        rate = `90%`,
        verbose = false
    }){
    return new Promise((resolve) => {
        checkParams(text_to_record, 'text_to_record');
        checkParams(file_name, 'text_to_record');

        fs.ensureDirSync(`${audio_folder}`);

        //set param tags
        const params = {
            'OutputFormat': 'mp3',
            'Text': `<speak><prosody rate="${rate}">${text_to_record}</prosody></speak>`,
            'VoiceId': voice_id,
            'Engine': 'neural',
            'LanguageCode': language_code,
            'TextType': 'ssml'
        }
        pollyRequest({params, audio_folder,file_name, verbose}).then(() => {
            resolve();
        }); //Creates the MP3 Files
    });
}

//Writes MP3 file based on the response from the Amazon Polly API.
async function pollyRequest({params, audio_folder,file_name, verbose}){
    return new Promise((resolve) => {
        Polly.synthesizeSpeech(params, (err, data) => {
            if(verbose) console.log(`Recording SSML string, ${params.Text}, with ${params.VoiceId}'s voice.`);
            if(err) throw new Error(`synthesizeSpeech Error: ${err}`);
            if(data){
                fs.writeFile(`${audio_folder}/${file_name}.mp3`, data.AudioStream, function(err) {
                    if (err) return err;
                    resolve();
                });
            }
        });
    });
};

module.exports = { createMP3 };