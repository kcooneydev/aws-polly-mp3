# MP3 Generator Toolkit

Create MP3's using Amazons Polly API

## Requirements

Node V8 or above
Amazon Client ID and Client Secret (Polly Authorised). [AWS CLI Setup](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

## Installation

```shell
npm install aws-polly-mp3
```

## API
```javascript
const {
    createMP3
} = require('../aws_polly_npm');

let text_to_record = `Rocco likes to sing along to music!`
let file_name = 'file_name'

createMP3({
    text_to_record,
    file_name
}).then(() => {
    //DO Something
})
```

## Contribute

This library is owned by cooneykarl, developed using the Amazon Docs for the Polly API.

Contributions are most welcome via pull requests.